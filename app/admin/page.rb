ActiveAdmin.register Page do
  controller do
    def permitted_params
      params.permit page: [:title, :url, :content]
    end
  end
end