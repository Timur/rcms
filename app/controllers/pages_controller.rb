class PagesController < InheritedResources::Base
  def show
    @page = Page.find_by_url(params[:url]) || not_found
  end
end
